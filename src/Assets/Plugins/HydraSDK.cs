using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;

/**
 * HydraSDK 
 *  - sixense Hydra SDK function mapping
 *  - call 'sixense.dll'
 *  - reference: sixense.h (in Hydra SDK)
 */
public class HydraSDK 
{
	[DllImport ("sixense")]
	public static extern int sixenseInit();

	[DllImport ("sixense")]
	public static extern int sixenseExit();
	
	[DllImport ("sixense")]
	public static extern int sixenseGetMaxBases();
	
	[DllImport ("sixense")]
	public static extern int sixenseSetActiveBase( int i );
	
	[DllImport ("sixense")]
	public static extern int sixenseIsBaseConnected( int i );

	[DllImport ("sixense")]
	public static extern int sixenseGetMaxControllers();
	
	[DllImport ("sixense")]
	public static extern int sixenseIsControllerEnabled( int which );
	
	[DllImport ("sixense")]
	public static extern int sixenseGetNumActiveControllers();

	[DllImport ("sixense")]
	public static extern int sixenseGetHistorySize();

	[DllImport ("sixense")]
	public static extern int sixenseGetData( int which, int index_back, ref sixenseControllerData s );
	
	[DllImport ("sixense")]
	public static extern int sixenseGetAllData( int index_back, ref sixenseAllControllerData s );
	
	[DllImport ("sixense")]
	public static extern int sixenseGetNewestData( int which, ref sixenseControllerData s );
	
	[DllImport ("sixense")]
	public static extern int sixenseGetAllNewestData( ref sixenseAllControllerData s );

	[DllImport ("sixense")]
	public static extern int sixenseSetHemisphereTrackingMode( int which_controller, int state );
	
	[DllImport ("sixense")]
	public static extern int sixenseGetHemisphereTrackingMode( int which_controller, ref int state );

	[DllImport ("sixense")]
	public static extern int sixenseAutoEnableHemisphereTracking( int which_controller );

	[DllImport ("sixense")]
	public static extern int sixenseSetHighPriorityBindingEnabled( int on_or_off );
	
	[DllImport ("sixense")]
	public static extern int sixenseGetHighPriorityBindingEnabled( ref int on_or_off );

	[DllImport ("sixense")]
	public static extern int sixenseTriggerVibration( int controller_id, int duration_100ms, int pattern_id );

	[DllImport ("sixense")]
	public static extern int sixenseSetFilterEnabled( int on_or_off );
	
	[DllImport ("sixense")]
	public static extern int sixenseGetFilterEnabled( ref int on_or_off );

	[DllImport ("sixense")]
	public static extern int sixenseSetFilterParams( float near_range, float near_val, float far_range, float far_val );
	
	[DllImport ("sixense")]
	public static extern int sixenseGetFilterParams( ref float near_range, ref float near_val, ref float far_range, ref float far_val );

	[DllImport ("sixense")]
	public static extern int sixenseSetBaseColor( byte red, byte green, byte blue );
	
	[DllImport ("sixense")]
	public static extern int sixenseGetBaseColor( ref byte red, ref byte green, ref byte blue );
}

/**
 * struct sixenseControllerData 
 *  - a struct to keep megnetic sensor controller data
 *  - size is 96 byte
 *  - struct layout is explicit, don't change anything.
 *  - reference: sixense.h  (in Hydra SDK)
 */
[StructLayout(LayoutKind.Explicit)]
public struct sixenseControllerData 
{
	[FieldOffset(0)] public float posx;   // position (x,y,z) relative to base
	[FieldOffset(4)] public float posy;
	[FieldOffset(8)] public float posz;
	
	[FieldOffset(12)] public float rot00; //OpenGL rotation matrix
	[FieldOffset(16)] public float rot01;
	[FieldOffset(20)] public float rot02;
	[FieldOffset(24)] public float rot10;
	[FieldOffset(28)] public float rot11;
	[FieldOffset(32)] public float rot12;
	[FieldOffset(36)] public float rot20;
	[FieldOffset(40)] public float rot21;
	[FieldOffset(44)] public float rot22;
		  
	[FieldOffset(48)] public byte joystick_x;
	[FieldOffset(49)] public byte joystick_y;
	[FieldOffset(50)] public byte trigger;
	
	[FieldOffset(52)] public uint buttons;
	[FieldOffset(56)] public byte sequence_number;
	
	[FieldOffset(60)] public float rot_quatx; // Quaternion
	[FieldOffset(64)] public float rot_quaty;
	[FieldOffset(68)] public float rot_quatz;
	[FieldOffset(72)] public float rot_quatw;
	
	[FieldOffset(76)] public UInt16 firmware_revision;
	[FieldOffset(78)] public UInt16 hardware_revision;
	[FieldOffset(80)] public UInt16 packet_type;
	[FieldOffset(82)] public UInt16 magnetic_frequency;
	
	[FieldOffset(84)] public int enabled;
	[FieldOffset(88)] public int controller_index;
	
	[FieldOffset(92)] public byte is_docked;
	[FieldOffset(93)] public byte which_hand;
	
	[FieldOffset(95)] public byte dummy;
}

	
[StructLayout(LayoutKind.Sequential)]
public struct sixenseAllControllerData 
{
	sixenseControllerData controller1;
	sixenseControllerData controller2;
	sixenseControllerData controller3;
	sixenseControllerData controller4;
}

