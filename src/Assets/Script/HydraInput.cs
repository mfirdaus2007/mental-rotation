using UnityEngine;
using System.Collections;

/**
 * HydraInput 
 *  - a Open Interface for Unity Application.
 *  - usage: 
 * 		HydraInput input = new HydraInput();
 *  	transform.rotation = input.GetRotation(0);
 * 		transform.position = input.GetPosition(0);
 */ 
public class HydraInput
{
	//private readonly int b = 1;
	private const int SUCCESS = 0;
	private const int FAILURE = -1;
	
	private bool IsHydraActive = false;
	
	public HydraInput() 
	{
		if(HydraSDK.sixenseInit() == SUCCESS) 
		{
			IsHydraActive = true;
			Debug.Log("Sixense Hydra Init Success!!");
		} else {
			IsHydraActive = false;
			Debug.Log("ERROR: Sixense Hydra Init Failed~");
		}
		
		if (IsHydraActive) 
		{						
			Debug.Log( "Number of Active Controllers:" + HydraSDK.sixenseGetNumActiveControllers());
			
			Debug.Log( "Controller 0 Is Active: " + HydraSDK.sixenseIsControllerEnabled(0) );
			Debug.Log( "Controller 1 Is Active: " + HydraSDK.sixenseIsControllerEnabled(1) );
		}
	}
	
	//@ by dos 
	public bool isConnected()
	{
		if(HydraSDK.sixenseGetNumActiveControllers() > 0)
			return true;
		else
			return false;
	}
	
	public Quaternion GetRotation(int which) 
	{
		if (HydraSDK.sixenseIsControllerEnabled(which) != 1)
			return Quaternion.identity; // not enabled now.	
				
		sixenseControllerData con0 = new sixenseControllerData();		
		HydraSDK.sixenseGetNewestData(which, ref con0);
				
		Quaternion q = new Quaternion(con0.rot_quatx, con0.rot_quaty, con0.rot_quatz, con0.rot_quatw);
		
		Quaternion q2 = Quaternion.Euler( -q.eulerAngles.x, -q.eulerAngles.y, q.eulerAngles.z);
		
		return q2;
	}	
	
	public Vector3 GetPosition(int which)
	{
		if (HydraSDK.sixenseIsControllerEnabled(which) != 1)
			return Vector3.zero; // not enabled now.
		
		sixenseControllerData con0 = new sixenseControllerData();		
		HydraSDK.sixenseGetNewestData(which, ref con0);
		
		return new Vector3(-con0.posx, -con0.posy, 0);
	}
	
	void OnApplicationQuit() 
	{
		// Don't call it,  Unity crash many times...
		// sixenseExit();
	}	
}
