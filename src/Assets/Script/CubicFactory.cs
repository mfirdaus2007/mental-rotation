using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

/**
 *  CubicFactory.cs
 *    - class conatins only one factory method.
 *    - cretae an Cubes array from file.
 *    - usage: 
 *      	List<Cubic> myCubic = CubucFactory.Load("DynamicInputData.txt");
 *			List<Cubic> myCubic = CubucFactory.Load("DynamicInputData.txt", Color.red);
 */
public class CubicFactory 
{	
	public static List<Cubic> Load( string filename )
	{
		return Load(filename, Color.white);
	}
	
	public static List<Cubic> Load( string filename , Color color)
	{
		List<Cubic> cubicList = new List<Cubic>();
		
		string fullpath = "MRVR_Data\\InputDynamicData\\" + filename;
		StreamReader file = new StreamReader(fullpath);
		
		string line;
		file.ReadLine(); // skip the first line;
		
		while ((line = file.ReadLine()) != null)
		{			
			if (String.IsNullOrEmpty(line.Trim()))
				break;			
			
			List<string> tokens = ParseLine(line);
			
			int id = int.Parse(tokens[0]);
			Cubic c = new Cubic( id );
			
			float tx,ty,tz, rx,ry,rz;
			tx = Convert.ToSingle(tokens[1]);
            ty = Convert.ToSingle(tokens[2]);
            tz = Convert.ToSingle(tokens[3]);
            rx = Convert.ToSingle(tokens[4]);
            ry = Convert.ToSingle(tokens[5]);
            rz = Convert.ToSingle(tokens[6]);
            			
			c.Translate(tx, ty, tz);
			c.Rotate(rx, ry, rz);
			
			c.SetColor( color );
			int cube_count = Convert.ToInt32(tokens[7]);
			
			// iterator each cube
			int index = 8;
			for(int i=0; i<cube_count; ++i)
			{							
				float bx, by, bz;
				bx = Convert.ToSingle(tokens[index]);
				by = Convert.ToSingle(tokens[index+1]);
				bz = Convert.ToSingle(tokens[index+2]);
									
				c.AddCube(bx, by, bz);				
				
				index += 3;
			}		
			cubicList.Add(c);
		}
		file.Close();
		
		return cubicList;
	}
	
	private static List<string> ParseLine( string line ) 
	{
		List<string> tokens = new List<string>();
		
		char[] delimiterChars = { ' ', ',', ':', '\t','\r','\n' };		
		string [] words = line.Split(delimiterChars);
		
		for (int k = 0; k < words.Length; k++)
		{
			if (words[k] != "")
			{
				tokens.Add(words[k]);
			}
		}
		return tokens;
	}
}
