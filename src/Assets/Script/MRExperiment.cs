﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

enum MRSTATE 
{
	PREPARE,
	RUNNING,
	BACK_TO_ZERO,
	FINISH,
}

public class MRExperiment : MonoBehaviour 
{			
	MRSTATE state = MRSTATE.PREPARE;
	int currentTest = -1;	
	
	List<Cubic> targetList;
	List<Cubic> testList;
	
	GameObject target;
	GameObject test;
	
	GUIText msg_center;	
	GUIText msg_buttom;
		
	ExpData data = new ExpData();
	MRConfig config = new MRConfig();
	HydraInput hydra = new HydraInput();
	MouseInput mouse = new MouseInput();
	
	Quaternion currentTestRot = Quaternion.identity;
	Vector3 currentTestPos = Vector3.zero;
	Vector3 currentHydraPos = Vector3.zero;
	
	float stop_time_counter = 0;
	float stop_time_interval = 0.5f;
	float time_show_cubic = 0;
		
	// ugly, modify later
	bool IsMatching = true;
	
	private float exp_begin_time = -1;
	
	void Awake ()
	{		
		targetList = CubicFactory.Load( config.dynamic_target_filename );
		testList   = CubicFactory.Load( config.dynamic_test_filename );		
		
		stop_time_interval = float.Parse(config["StopTimeInterval"]);
	}
		
	void Start () 
	{						
		msg_center = GameObject.Find("msg_center").guiText;
		msg_center.text = "";	
		msg_buttom = GameObject.Find("msg_bottom").guiText;
		msg_buttom.text = "";
	}
		
	void Update () 
	{							
		CheckExitApplication();
		
		if (state == MRSTATE.PREPARE) {
						
			msg_center.text = "正式實驗即將開始，請按'O'。\nPlease press 'O' to Start Experiment.";
			
			if (Input.GetKeyDown(KeyCode.V)) {
				InitExpTimer();
				msg_center.text = "";
				state = MRSTATE.RUNNING;
				NextTest();
			}
			
		} else if (state == MRSTATE.RUNNING) { // Experiment is still running 	
			
			ControllCubic();
			MakeDecision();
			
		} else if (state == MRSTATE.BACK_TO_ZERO ) {
			
			if (target != null) {
				targetList[currentTest].Destroy();
				testList[currentTest].Destroy();
			}
			
			stop_time_counter += Time.deltaTime;
			if (stop_time_counter > stop_time_interval ) {				
				stop_time_counter = 0;
				
				state = MRSTATE.RUNNING;
				NextTest();			
			}			
		} else if (state == MRSTATE.FINISH) {
			
			msg_center.text = "實驗結束，感謝您的參與\nFinish, Thank you.";
			
			if (Input.anyKeyDown)
			{
				Debug.Log("Quit Application!");
				Application.Quit();
			} 			
			
		}
	}
	
	private void InitExpTimer() 
	{
		if (exp_begin_time < 0) 
		{
			exp_begin_time = Time.time;
			Debug.Log("Begin!" + exp_begin_time);			
		}		
	}
	
	private float ExpTimer()
	{		
		return (Time.time - this.exp_begin_time);
	}
	
	private void ControllCubic() 
	{
		if (config["InputMode"] == "HydraTracker")
		{			
			test.transform.rotation = hydra.GetRotation(0) * currentTestRot;
			
			Vector3 vpos =  (hydra.GetPosition(0) - currentHydraPos) * 0.02f;
			test.transform.position = currentTestPos + vpos;
			
			data.LogEuler(currentTest, ExpTimer(), test.transform.rotation.eulerAngles);
		}		
		
		if (config["InputMode"] == "Mouse") 
		{
			mouse.MoveCubic(this.transform);
			data.LogEuler(currentTest, ExpTimer(), test.transform.rotation.eulerAngles);
		}
	}
	
	private void MakeDecision() 
	{	
	/*
		if (IsMatching)
		{
			if (Vector3.Distance(target.transform.position, test.transform.position) < 0.1)
			{
				if (Vector3.Distance(target.transform.forward, test.transform.forward) < 0.18) 
				{
					//Debug.Log("GO!!!!");
					state = MRSTATE.BACK_TO_ZERO;		
				}
			}
			//Debug.Log(Vector3.Distance(target.transform.position, test.transform.position));
			
			return;
		}
		*/
		string UserAnswer = "";
		if (Input.GetKeyDown(KeyCode.V)) {
			UserAnswer = "Same";
		} else if (Input.GetKeyDown(KeyCode.Z)) {
			UserAnswer = "Diff";
		}
		
		if (Input.GetKeyDown(KeyCode.V) || Input.GetKeyDown(KeyCode.Z))
		{
			// Log 
			data.LogDecision(
				currentTest,
			    time_show_cubic,
			    ExpTimer(), 
			    targetList[currentTest].ID, 
			    testList[currentTest].ID, 
			    targetList[currentTest].Rotation(),
			    testList[currentTest].Rotation(),
			    UserAnswer // depends on V/Z
			);
			
			state = MRSTATE.BACK_TO_ZERO;			
		} 		
	}
	
	private void NextTest () 
	{				
		currentTest++;	
		
		msg_buttom.text = (currentTest+1).ToString() + "/80";
		
		if (currentTest >= targetList.Count ) {		
			state = MRSTATE.FINISH;	
			return;
		}
		target = targetList[currentTest].Create();		
		test =  testList[currentTest].Create();
				
		time_show_cubic = ExpTimer();
		
		currentTestRot = Quaternion.Inverse(hydra.GetRotation(0));
		currentTestRot *= test.transform.rotation;
		
		currentHydraPos = hydra.GetPosition(0);
		currentTestPos = test.transform.position;
	}	
	
	void OnApplicationQuit ()
	{		
		data.Close();
	}	
	
	private void CheckExitApplication() 
	{
		if (Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}
	}	
}
