using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

/**
 * MRConfig.cs
 */
public class MRConfig {
	
	private Dictionary<string, string> config_data = new Dictionary<string, string>();
	
	public string input_mode = "HydraTracker";  // HydraTracker/Mouse/Static/CameraTracker
	public string display_mode = "Mono";		// Mono/3DVision
	public string dynamic_target_filename = "";
	public string dynamic_test_filename = "";	
	public int thresh = 5;
	
	public MRConfig() {
		Load("MRVR_DATA\\MRVR_config.txt");
	}
			
	public void Load( string filename ) {
				
		using (StreamReader fin = File.OpenText(filename)) 
		{
			string line;
			while( (line=fin.ReadLine()) != null ) {
				
				List<string> tokens = ParseLine(line);
				config_data[tokens[0]] = tokens[1];
				
				Debug.Log("Config ["+ tokens[0] +"] = ["+ tokens[1] +"]");
			}
		
			this.input_mode = config_data["InputMode"];
			this.display_mode = config_data["DisplayMode"];
			this.dynamic_target_filename = config_data["Dynamic_TargetFileName"];
			this.dynamic_test_filename = config_data["Dynamic_TestFileName"];
			
			this.thresh = int.Parse(config_data["Thresh"]);
		}
	}
	
	public string this [string index] {
	
		get {
			return config_data[index];
		}
	}
	
	private List<string> ParseLine(string line)
    {					
		char[] delimiterChars = { ' ', '\t','\r','\n' };
			
		string [] words = line.Split(delimiterChars);
	
		List<string> tokens = new List<string>();
		for (int k = 0; k < words.Length; k++)
		{
			if (!string.IsNullOrEmpty(words[k]))
			{				
				tokens.Add(words[k]);
			}
		}
		return tokens;
	}
}
