﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * 
 * 
 * 
 */
public class MRTraining : MonoBehaviour {
	
	private MRSTATE state = MRSTATE.PREPARE;
	
	private int currentTest = -1;	
		
	private List<Cubic> targetList;
	private List<Cubic> testList;	
	
	private GUIText msg_center;	
	private GUIText msg_bottom;
	
	private GameObject target;
	private GameObject test;
		
	private MRConfig config = new MRConfig();
	private HydraInput hydra = new HydraInput();	
	private MouseInput mouse = new MouseInput();
	
	private Quaternion currentTestRot = Quaternion.identity;
	
	private float stop_time_interval = 1f;
	private float stop_time_counter = 0;	
	
	void Awake() 
	{
		targetList = CubicFactory.Load("Training_TargetData.txt");
		testList   = CubicFactory.Load("Training_TestData.txt");
		
		msg_center = GameObject.Find("msg_center").guiText;				
		msg_bottom = GameObject.Find("msg_bottom").guiText;		
		
		stop_time_interval = float.Parse(config["StopTimeInterval"]);
	}
	
	// Use this for initialization
	void Start () 
	{
		state = MRSTATE.PREPARE;		
	}
	
	// Update is called once per frame
	void Update () {
			
		CheckExitApplication();
		
		if (state == MRSTATE.PREPARE) {
			
			msg_center.text = "請按O開始練習題\n Please Press 'O' to Start the exercise.";
			
			if ( Input.anyKeyDown ) {
				msg_center.text = "";
				
				NextTest();
				msg_bottom.text  = "請判斷左右兩方塊是否相同，如果你認為相同，請按O，認為不同，請按X。\n";
				msg_bottom.text += "Please Answer the two cubics are the same or different, Press 'O' if the same, Press 'X' if different";
				state = MRSTATE.RUNNING;
			}			
			
		} else if (state == MRSTATE.RUNNING) {
						
			ControllCubic();
			
			if (Input.GetKeyDown(KeyCode.V) || Input.GetKeyDown(KeyCode.Z)) 
			{				
				CheckUserAnswer();	
				state = MRSTATE.BACK_TO_ZERO;
			} 
			
		} else if (state == MRSTATE.BACK_TO_ZERO) {
			
			if (target != null) {
				GameObject.Destroy(target);
				GameObject.Destroy(test);
			}
			
			stop_time_counter += Time.deltaTime;
			
			if ( IsReadyNextTest() ) {			
				stop_time_counter = 0;
				
				state = MRSTATE.RUNNING;
				msg_center.text = "";
				NextTest();				
			}
				
		} else if (state == MRSTATE.FINISH) {
			
			msg_center.text =  "請問對測驗或介面操作有什麼問題嗎? 如果沒有，請按 'O' 鍵 進入正式實驗\n";
			msg_center.text += "Do you have any question? If No, Press 'O' to Continue.";
			
			msg_bottom.text = "";
			
			if (Input.GetKeyDown(KeyCode.V)) {
				Application.LoadLevel("MR_Experiment");
			} else if (Input.GetKeyDown(KeyCode.Z)) {
				currentTest = -1;
				state = MRSTATE.PREPARE;
			}
		}
	}
	
	private bool IsReadyNextTest() {
		
		bool IsReady = true;
		
		IsReady = IsReady && (stop_time_counter > stop_time_interval );
		
		//if (config["InputMode"] == "HydraTracker") {
		//	Debug.Log(hydra.GetRotation(0).eulerAngles);
		//}
			
		return IsReady;		
	}
	
	private void NextTest() 
	{
		currentTest++;
				
		if (currentTest >= targetList.Count) {
			state = MRSTATE.FINISH;
			return;
		}		
		
		target = targetList[currentTest].Create();
		test = testList[currentTest].Create();	
				
		currentTestRot = Quaternion.Inverse(hydra.GetRotation(0));
		currentTestRot *= test.transform.rotation;
	}
	
	private void ControllCubic() 
	{
		if (config["InputMode"] == "HydraTracker")
		{			
			test.transform.rotation = hydra.GetRotation(0) * currentTestRot;						
		}
		
		if (config["InputMode"] == "Mouse") 
		{			
			test.transform.Rotate(mouse.GetRotationEuler(), Space.World);
		}
	}
	
	private void CheckUserAnswer() 
	{
		bool UserAnswer = (Input.GetKeyDown(KeyCode.V))? true : false;
		bool CorrectAnswer = (targetList[currentTest].ID == testList[currentTest].ID);
		
		if (UserAnswer == CorrectAnswer) {
			msg_center.text = "答對了!\n Correct!";			
		} else {
			msg_center.text = "答錯了~\n Wrong Answer~";			
		}		
	}
	
	private void CheckExitApplication() 
	{
		if (Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}
	}	
}
