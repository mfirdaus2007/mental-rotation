/**
    SimpleDictionary
    Author: Tonio Loewald
    Date: 4/27/2009
    
    Implements a simple key/value dictionary (of strings)
    Also allows loading and saving of dictionaries from text files
    "=" should not be used in key strings!
    
    Usage:
    var d = new SimpleDictionary();
    
    d.Set( "foo", "bar" );
    print( d.Get( "foo" ) ); // "bar"
    
    d.Set( "bar", "baz" );
    d.Set( "foo", "blah" );
    print( d.Get( "foo" ) ); // "blah"
    
    d.Remove( "foo" );
    print( d.Count() ); // 1
    d.Set( "foxtrot", "uniform" );
    d.Save( "test.ini" ); // file will be bar=baz\nfoxtrot=uniform\n
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

class SimpleDictionary : ScriptableObject {

	public Dictionary<string, string> hash = new Dictionary<string, string>();
    
    string Get( string key ) {

    	string value;
    	if (hash.TryGetValue(key, out value) == false)
    	{
			return "";    		
    	} 
        return value;
    }

    void Set( string key, string value ) {
        hash[key] = value;
    }
    
    void Remove( string key ) {
    	if (hash.Remove(key) == false) {  
        	Debug.Log( "SimpleDictionary.Remove failed, key not found: " + key );
        }
    }

    void Save(string fileName) {
        StreamWriter sw = new StreamWriter ( Application.dataPath + "/" + fileName );
        foreach (var item in hash){
            sw.WriteLine( item.Key + "=" + item.Value);
        }
        sw.Close ();
        Debug.Log ( "SimpleDictionary.Saved " + Application.dataPath + "/" + fileName );
    }

    void Load( string fileName ) {
 		
 		hash.Clear();

        try {
            using (StreamReader sr = File.OpenText( Application.dataPath + "/" + fileName)) {
            
        		string line = "-";
        		int offset;

        		while ((line = sr.ReadLine()) != null) {
            		offset = line.IndexOf("=");
                	if (offset > 0) {
                    	Set( line.Substring(0, offset).Trim(), line.Substring(offset+1).Trim() );
                	}                
            	}                                	
        	}
        	Debug.Log ( "SimpleDictionary.Loaded " + Application.dataPath + "/" + fileName );
        }
        catch (Exception ex) {
            Debug.Log ( "SimpleDictionary.Load failed: " + Application.dataPath + "/" + fileName );
        }
    }

    int Count(){
        return hash.Count;
    }
}