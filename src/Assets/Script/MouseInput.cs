using UnityEngine;
using System.Collections;

/**
 * 
 * 
 * 
 */ 
public class MouseInput {

	private Vector3 last_position;
	private Quaternion current_rotation;
	
	public MouseInput () 
	{		
		current_rotation = Quaternion.identity;
	}
	
	// not work now.
	// Use GetRotationEuler() instead.
	/*
	public Quaternion GetRotation () 
	{
		if (Input.GetButtonDown("Fire1")) 
		{
			last_position = Input.mousePosition;
		}
		
		if (Input.GetButton("Fire1")) 
		{
			Vector3 diff = Input.mousePosition - last_position;
			if (diff != Vector3.zero)
			{
				current_rotation *= Quaternion.AngleAxis( diff.y, Vector3.right);
				current_rotation *= Quaternion.AngleAxis(-diff.x, Vector3.up);
	
				last_position = Input.mousePosition;				
			}			
		}
		return current_rotation;
	}
	*/
	
	// usage:  transform.Rotate(GetRotationEuler(), Space.World);
	public Vector3 GetRotationEuler() 
	{
		if (Input.GetButtonDown("Fire1")) 
		{
			last_position = Input.mousePosition;
		}
		Vector3 diff = Vector3.zero;
		if (Input.GetButton("Fire1")) 
		{
			diff = Input.mousePosition - last_position;
			last_position = Input.mousePosition;			
		}
		return new Vector3(diff.y, -diff.x, 0);
	}
	
	public void MoveCubic(Transform transform)
	{
		if (Input.GetButtonDown("Fire1")) 
		{
			last_position = Input.mousePosition;
		}
		Vector3 diff = Vector3.zero;
		if (Input.GetButton("Fire1")) 
		{
			diff = Input.mousePosition - last_position;
			last_position = Input.mousePosition;			
		}
		
		transform.Rotate(new Vector3(diff.y, -diff.x, 0), Space.World);				
	}
}
