using UnityEngine;
using System;
using System.IO;
using System.Collections;

/**
 * ExpData.cs
 *   - Collect 2 type of data 
 *     1. User's Decision, Target/Test Cubic is Same or Different.
 *     2. The Rotate guesture (x,y,z) of Tracker 
 *   The output file is put in ExpData/ directory.
 */ 
public class ExpData {

	private StreamWriter fp_euler = null;
	private StreamWriter fp_decision = null;	
	
	float last_log_time;
	
	public ExpData() 
	{
		if (!Directory.Exists("ExpData")) {
			Directory.CreateDirectory("ExpData");
		}
		
		last_log_time = 0;
	}
	
	// close and flush file stream.
	public void Close() 
	{
		if (fp_euler != null) {
			fp_euler.Flush();
			fp_euler.Close();
			fp_euler = null;
		}
		
		if (fp_decision != null) {
			fp_decision.Flush();
			fp_decision.Close();
			fp_decision = null;
		}
		
		Debug.Log("Log Close!");
	}
	
	// Log a user's decision, Target/Test is same or different.
	public void LogDecision (int test_no, float time_begin, float time, int target_ID, int test_ID, Vector3 target_Rot, Vector3 test_Rot, string UserAnswer)
	{
		if (fp_decision == null)
		{
			create_decision_file();
		}		
		
		fp_decision.Write( "{0},", test_no);
		fp_decision.Write( "{0},", F2(time_begin));
		fp_decision.Write( "{0},", F2(time));
		
		fp_decision.Write( "{0},", target_ID);
		fp_decision.Write( "{0},{1},{2}, ", F2(target_Rot.x), F2(target_Rot.y), F2(target_Rot.z));
		
		fp_decision.Write(round45(target_Rot.y) + ",");
		
		fp_decision.Write( "{0},", test_ID);
		fp_decision.Write( "{0}, {1}, {2},", F2(test_Rot.x), F2(test_Rot.y), F2(test_Rot.z));
		fp_decision.Write( "{0},", round45(test_Rot.y));
		
		string CorrectAnswer =  (target_ID==test_ID)? "Same" : "Diff";	
	
		fp_decision.Write("{0},", CorrectAnswer);
		fp_decision.Write("{0}" , UserAnswer); // Same/Diff
			
		fp_decision.Write("\n");
	}
	
	// Log current rotation of Target/Test cubics.
	public void LogEuler (int test_no, float time, Vector3 rot)
	{	
		if (fp_euler == null) { 
			create_euler_file(); 		
		}
		
		if( time - last_log_time  > 0.045) {  //log per 45ms
				
			fp_euler.Write("{0},", test_no);
			fp_euler.Write("{0},", time.ToString("0.000"));
			fp_euler.Write("{0}, {1}, {2}", F2(rot.x), F2(rot.y), F2(rot.z));
		
			fp_euler.Write("\n");
	
			last_log_time = time;
		}
	}
	
	private void create_decision_file() 
	{
		string expNo = getCurrentExpNo();	
		Debug.Log( "Current expNo: " + expNo);
	
		string filename = "ExpData\\" + expNo + "_decision_" + Now() + ".csv";
		
		fp_decision = File.CreateText(filename);		
				
		// the csv head line
		fp_decision.Write("trial No, Begin Time(Sec), Decision Time(Sec),");
		fp_decision.Write("Target ID, Target RX, Target RY, Target RZ, Target Orignial RY,");
		fp_decision.Write("Test ID,   Test RX,   Test RY,   Test RZ, Test Orignial RY,");
		fp_decision.Write("Correct Answer, User Answer");
		
		fp_decision.Write("\n");
	}
			
	private void create_euler_file() 
	{
		string expNo = getCurrentExpNo();

		string filename = "ExpData\\" + expNo + "_euler_" + Now() + ".csv";
		fp_euler = File.CreateText(filename);		

		// the csv head line
		fp_euler.Write("trial No, Time(Sec),  X, Y, Z\n");		
	}
	
	// convert float to string with only 2 demical point.
	private string F2( float f )
	{
		return f.ToString("0.00");
	}
	
	/**
	 *  Round degree to one of 0,45,90,135,180,225,270,315
	 */
	private int round45(float ry) 
	{
		int diff = 11;
	
		for (int i=0;i<=360; i+=45)
		{
			float up_bound = i + diff;
			float low_bound = i - diff;
			if ( ry > low_bound && ry < up_bound )
			{
				return i;
			}
		}
		return 0;
	}
	
	private string getCurrentExpNo() 
	{
		string expNo = "";
		using( StreamReader fin = File.OpenText("ExpData\\expNo.tmp")) {
			expNo = fin.ReadLine();
		}	
		return expNo;	
	}
	
	private string Now() 
	{	
		return DateTime.Now.ToString("yyyyMMdd_HHmm");		
	}
}
