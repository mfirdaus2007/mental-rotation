﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/**
 * Cubic.cs
 *	 - a Set of Cubes
 *
 *
 */ 
public class Cubic
{
	private GameObject root; 		// a parent GameObject for all cubes			
	private List<Vector3> posList; 	// position of each cube	
	private Texture2D tex;			// Texture
	
	private Vector3 position;		// the position of this Cube set
	private Vector3 rotation;
	private Vector3 scaling;	
	
	public int ID;
	
	
	public Cubic( int _ID ) 
	{		
		this.ID = _ID;
		this.posList = new List<Vector3>();
		this.position = Vector3.zero;
		this.rotation = Vector3.zero;
		this.scaling  = Vector3.one;
	}	
		
	public GameObject Create()
	{
		
		GameObject center = new GameObject();
		foreach ( Vector3 pos in posList )
		{
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube) ;
			cube.renderer.material.mainTexture = this.tex;
			cube.transform.position = pos;			
			
			// @mat: append child 
			cube.transform.parent = center.transform;
		}
		
		center.transform.Translate(-AvgPosition(center.transform), Space.World);
				
		this.root = new GameObject();		
		center.transform.parent = this.root.transform;
		
		root.transform.Translate(this.position, Space.World); //position = this.position;
		root.transform.Rotate(this.rotation);
		//root.transform.localScale = this.scaling;
		root.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
		
		return root;
	}
	
	private Vector3 AvgPosition(Transform tparent) {
		
		Vector3 result = Vector3.zero;
		
		foreach(Transform t in tparent) {
			result += t.position;
		}
		result = result / tparent.childCount;
		
		return result;
	}
			                           
			                           
	public void Destroy()
	{	
		GameObject.Destroy(root);		
	}
	
	public void AddCube(float x, float y, float z) 
	{
		posList.Add(new Vector3(x,y,z));
	}
			
	public void Translate( float x, float y, float z) 
	{		
		this.position = new Vector3(x,y,z);		
	}
	public void Scale( float x, float y, float z) 
	{
		this.scaling =  new Vector3(x,y,z);
	}
	public void Rotate( float x, float y, float z) 
	{
		this.rotation = new Vector3(x,y,z);
	}
	
	public Vector3 Position() {
		return this.position;
	}
	public Vector3 Rotation() {
		return this.rotation;
	}
	
	public void SetColor( Color clr ) {
		this.tex = fill(clr);
	}
	
	private Texture2D fill( Color clr)
	{
		Color color;
		Texture2D texture = new Texture2D(128, 128);
		int y = 0;
		
		while (y < texture.height) {
			int x = 0;
			while (x < texture.width) {
				if(x == 0 || y == 0 || x == 127 || y == 127) 
					color = Color.black;
				else						
					color = clr;
				texture.SetPixel(x, y, color);
				++x;
			}
			++y;
		}
		texture.Apply();
		return texture;
	}
}
