using UnityEngine;
using System.Collections;
using System.IO;
using System;
using System.Collections.Generic;

public class Cubes
{
	private GameObject cube_parent;
	//@mat: i create a parent GameObject for all cubes
	private GameObject [] cube;
	private int n;
	private float absX,absY,absZ;
	public void destroy()
	{

		for(int i=0;i<n;i++)
			{
				GameObject.Destroy(cube[i]);
			}
		//@mat
		GameObject.Destroy(cube_parent);
	}
	
	public Cubes(int _n, Texture2D txtr)
	{
			n=_n;			
			cube_parent = new GameObject();  
			cube = new GameObject[n];
			for(int i=0;i<n;i++)
			{
				cube[i] = GameObject.CreatePrimitive(PrimitiveType.Cube) ;					
				cube[i].renderer.material.mainTexture = txtr;
				
				// @mat: add child 
				cube[i].transform.parent = cube_parent.transform;
			}
	}
	public Cubes(int _n)
	{
			cube_parent = new GameObject();  
			n=_n;
			cube = new GameObject[n];
			for(int i=0;i<n;i++)
			{
				cube[i] = GameObject.CreatePrimitive(PrimitiveType.Cube) ;
			}
	}
	public void partTranslation(int _n, float x, float y, float z)
	{		
		cube[_n].transform.Translate(x,y,z);		
	}
	// @mat: i add two new method to control whole cubic
	public void translate( float x, float y, float z) 
	{		
		cube_parent.transform.position = new Vector3(x,y,z);		
		absX=x;		absY=y;		absZ=z;
	}
	public void scale( float x, float y, float z) 
	{
		cube_parent.transform.localScale =  new Vector3(x,y,z);
	}
	public void rotate( float x, float y, float z) 
	{
		cube_parent.transform.Rotate(x,y,z);
	}
	public GameObject getRoot()
	{
			return cube_parent;
	}
	public void fixPosition()
	{
		float minZ=100000,maxZ=-100000;
		float minY=100000,maxY=-100000;
		float minX=100000,maxX=-100000;
		float x,y,z;
		for(int i=0;i<n;i++)
		{
			x=cube[i].transform.position.x;
			y=cube[i].transform.position.y;
			z=cube[i].transform.position.z;
			if(x>maxX)
				maxX=x;
			if(y>maxY)
				maxY=y;
			if(z>maxZ)
				maxZ=z;
			
			if(x<minX)
				minX=x;
			if(y<minY)
				minY=y;
			if(z<minZ)
				minZ=z;			
		}
		float midX=(maxX-minX)/2;
		float midY=(maxY-minY)/2;
		float midZ=(maxZ-minZ)/2;
		Debug.Log(new Vector3(midX,midY,midY));
		cube_parent.transform.position = new Vector3(absX+midX,absY+midY,absZ+midZ);		
	}
	public void control(Quaternion h)
	{
		cube_parent.transform.rotation=h;
	}
}

public class mrinit : MonoBehaviour {
	bool help=true;
	HydraInput hydra;
	Cubes targetCubes, inputCubes;
	string inputFile="Dynamic_TargetData_Mat.txt";				
	string targetFile="Dynamic_TestData_Mat.txt";
	Color cTarget = new Color(1f,0.49f,0.4f);
	Color cInput = new Color(0.8f,0.4f,0.85f);
	
	int c,count,max=1000;
	char[] delimiterChars = { ' ', ',', '.', ':', '\t','\r','\n' };
	int testnum;
	public Rect windowRect;
	// Use this for initialization
	
	public void Awake()
	{
		hydra=new HydraInput();
	}
	
	public Texture2D fill( Color clr)
	{
		Color color;
		Texture2D texture = new Texture2D(128, 128);
		int y = 0;
				while (y < texture.height) {
				int x = 0;
					while (x < texture.width) {
						if(x == 0 || y == 0 || x == 127 || y == 127) 
							 color = Color.white;
						else						
							 color = clr;
						texture.SetPixel(x, y, color);
						++x;
					}
				++y;
				}
				texture.Apply();
				return texture;
	}
	
	Cubes readInput(int num, string filename,Color warna)
	{
		Cubes snake=new Cubes(0);		
		string namafile = filename;
		StreamReader file = new System.IO.StreamReader(namafile);
		Texture2D txtr = fill(warna);
		
		string line;
		int i=0;
		float rx,ry,rz,tx,ty,tz,bx,by,bz;
		List<string> buff ;
		while ((line = file.ReadLine()) != null)
        {
			if(i==num)
			{
				buff = new List<string>();
				string [] words = line.Split(delimiterChars);
				
				int k;
                for (k = 0; k < words.Length; k++) 
                {
                    if (words[k] != "")
                    {
                        buff.Add(words[k]);
                    }
                }
				//num = Convert.ToInt16(buff[0]);
                tx = Convert.ToSingle(buff[1]);
                ty = Convert.ToSingle(buff[2]);
                tz = Convert.ToSingle(buff[3]);
                rx = Convert.ToSingle(buff[4]);
                ry = Convert.ToSingle(buff[5]);
                rz = Convert.ToSingle(buff[6]);
                count = Convert.ToInt16(buff[7]);
				
				c = 0;
				
				snake = new Cubes(count, txtr);
				
				int j=8;
				while (c < count)
				{			
					bx =	Convert.ToSingle(buff[j]);
					by =	Convert.ToSingle(buff[j+1]);
					bz =	Convert.ToSingle(buff[j+2]);
									
					snake.partTranslation(c, bx, by, bz);
					
					c++;					
					j+=3;
				}
				
				// @mat: translat & rotatoe on parent GameObject
				//Debug.Log( "Tanslate:" + new Vector3(tx, ty, tz));
				//Debug.Log( "Rotate:"   + new Vector3(rx, ry, rz));
				
				//snake.scale(0.65f,0.65f,0.65f);
				snake.rotate(rx, ry, rz);
				snake.translate(tx,ty,tz);
				//snake.fixPosition();
			}
			i++;			
			// Do what you want with the line of text read from the file here...
        }		
        file.Close();
		max=i;
		return snake;
	}
		

	void OnGUI() {
		if(help)
		windowRect = 
		GUI.Window(0, new Rect(20, 20, 180, 260), DoMyWindow, "Help (F1)");
	}
	
	void DoMyWindow(int windowID) {
		
		GUILayout.Label ("Press V - Simillar");
		GUILayout.Label ("Press Z - Different");
		GUILayout.Label ("W - Pitch Up");
		GUILayout.Label ("S - Pitch Down");
		GUILayout.Label ("A - Yaw Left");
		GUILayout.Label ("D - Yaw Right");
		GUILayout.Label ("Q - Roll Clockwise");
		GUILayout.Label ("E - Roll Counter Clockwise");
		
		if(!help)
			Destroy(this);
	}
	
	void Start () {
		testnum=1;		
		targetCubes=readInput(testnum,targetFile,cTarget);
		inputCubes = readInput(testnum,inputFile,cInput);
		
	}
	
	// Update is called once per frame
	void Update () {
		if(hydra.isConnected())
			inputCubes.control(hydra.GetRotation(0));
		
		if (Input.GetKeyDown(KeyCode.F1))
		{
			if(help)
				help=false;
			else
				help=true;
		}
			if (Input.GetKeyDown(KeyCode.V)||Input.GetKeyDown(KeyCode.Z))
			{
			Cubes buff;
			buff = targetCubes;
			buff.destroy();
			buff = inputCubes;
			buff.destroy();
			/*	
			if(testnum==1)			
				testnum=max-1;				
			else
				testnum--;
				*/
			if(testnum>max-2)			
				testnum=1;				
			else
				testnum++;	
			targetCubes=readInput(testnum,targetFile,cTarget);			
			inputCubes=readInput(testnum,inputFile,cInput);				
			}
			if (Input.GetKey(KeyCode.Q))			
				inputCubes.rotate(0,0,-1);
			if (Input.GetKey(KeyCode.E))
				inputCubes.rotate(0,0,1);			
			if(Input.GetKey(KeyCode.S))				
				inputCubes.rotate(-1,0,0);
			if(Input.GetKey(KeyCode.W))
				inputCubes.rotate(1,0,0);
			if(Input.GetKey(KeyCode.A))
				inputCubes.rotate(0,-1,0);
			if(Input.GetKey(KeyCode.D))
				inputCubes.rotate(0,1,0);
			
	}
	
}
